package com.qf.project.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.project.entity.Users;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<Users> {

}