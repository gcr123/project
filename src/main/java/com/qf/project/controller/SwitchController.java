package com.qf.project.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.gson.Gson;
import com.qf.project.dao.UserMapper;
import com.qf.project.entity.Users;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class SwitchController {

    @Resource
    UserMapper userMapper;

    //关于页面跳转 静态页面必须在static里面 动态页面在templas 里面 访问带后缀的直接访问，不带后缀默认会给一个.ftl的页面
    @RequestMapping("/fre")
    public ModelAndView test() {
        ModelAndView m = new ModelAndView("index1");
        m.addObject("user", "gcr");
        return m;
    }

    @RequestMapping("/thy")
    public ModelAndView thymeleaf(Model model) {
        ModelAndView m = new ModelAndView("index2");
        m.addObject("user", "gcr");
        return m;
    }

    @RequestMapping("/welcome")
    public String wel() {
        return "index1";
    }

    @RequestMapping("/index2")
    public String test2() {
        return "index2.html";
    }

    @RequestMapping("/img")
    @ResponseBody
    public String test3() {
        return "/img/sm.jpg";
    }

    @RequestMapping("/")
    @ResponseBody
    public String welcome() {
        return "欢迎来到Jenkins 自动部署项目环节，当前页面是自动部署的";
    }

    @RequestMapping("/find")
    @ResponseBody
    public String findUser(Integer rows, Integer page) {
        List<Users> list;
        list = null;
        Map<String, Object> map = new HashMap<String, Object>();
        //默认第一页 一页十个
        if (rows == null) {
            rows = 2;
        }
        if (page == null) {
            page = 1;
        }

//        Page<Users> us = new Page<>(page,rows);
//        QueryWrapper<Users> queryWrapper =  new QueryWrapper<>();
//        queryWrapper.orderByAsc("age");
//        IPage<Users> iPage = userMapper.selectPage(us,queryWrapper);
//        return  new Gson().toJson(iPage);


        //第二种方式测试
        PageHelper.startPage(page, rows);
        list = userMapper.selectList(null); //查询数据用的
        PageInfo<Users> pages = new PageInfo<Users>(list);
        map.put("rows", list);
        map.put("total", pages.getTotal());
        return new Gson().toJson(map);
    }

}
